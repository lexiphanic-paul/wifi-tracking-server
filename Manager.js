const LocalDevices = require('local-devices');
const Mac = require('mac-address');
const MESSAGE_MAP = {
    0x02: "getTrackers",
    0x04: "assign",
    0x06: "addReport",
    0x08: "viewDevices",
    0x0a: "train",
    0x0c: "restart",
    0x0e: "setChannel",
};

class Manager {
    constructor(wss) {
        this._wss = wss;
        this._wss.on('connection', (ws) => {
            this._handleConnection(ws);
        });
        this._trackers = [];
        this._devices = {};
        this._monitors = [];
        this._trainingData = [];
        this._boot();
    }

    async _boot () {
        const currentDevices = await LocalDevices();

        for (let currentDevice of currentDevices) {
            let device = this._getDevice(currentDevice.mac);

            if (currentDevice.name && currentDevice.name !== '?') {
                device.name = currentDevice.name;
            }

            if (currentDevice.ip && currentDevice.ip !== '?') {
                device.ip = currentDevice.ip;
            }
        }
    }

    async _handleConnection(ws) {
        console.log("New Connection...");
        ws.on('message', (message) => {
            this._handleMessage(ws, message);
        });

        const arpData = await LocalDevices(ws._socket.address().address)
        if (arpData) {
            console.log(arpData, Mac.toBuffer(arpData.mac));
            this._assign(ws, Mac.toBuffer(arpData.mac));
            console.log('    ...auto Assigned.');
        }
    }

    _handleMessage(ws, message) {
        if (typeof message === "string") {
            message = Buffer.from(message);
        }

        const messageType = message[0];

        if (!MESSAGE_MAP[messageType]) {
            console.error('Unknown message type: ' + messageType);

            return;
        }
        console.log("Message Type: " + MESSAGE_MAP[messageType]);

        try {
            console.log(message);
            this["_" + MESSAGE_MAP[messageType]](ws, message.slice(1));
        } catch (e) {
            console.error(e);
        }
    }

    _getTrackers(ws, message) {
        let result = [0x03];

        for (let tracker of this._trackers) {
            result.push(tracker.whoami[0]);
            result.push(tracker.whoami[1]);
            result.push(tracker.whoami[2]);
            result.push(tracker.whoami[3]);
            result.push(tracker.whoami[4]);
            result.push(tracker.whoami[5]);
        }

        ws.send(Buffer.from(result));
    }

    _assign(ws, message) {
        if (message.length !== 6) {
            console.log('Message length is incorrect: ' + message.length);

            return;
        }

        ws.whoami = message;
    }

    _addReport(ws, message) {
        if (!ws.whoami) {
            //return;
        }

        if (message.length < 14) {
            console.log('Message length is incorrect: ' + message.length);

            return;
        }

        if (ws.isTracker !== true && this._trackers.indexOf(ws) === -1) {
            this._trackers.push(ws);
            ws.isTracker = true;
        }
        ws.trackerLastUpdated = new Date();

        const source = message.slice(0, 6);
        const sourceHuman = Mac.toString(source);
        const target = message.slice(6, 11);
        const targetHuman = Mac.toString(target);
        const channel = message.slice(12, 13).readUInt8();
        const rssi = message.slice(13, 14).readInt8();

        const device = this._getDevice(sourceHuman);

        if (!device['targets'][targetHuman]) {
            device['targets'][targetHuman] = 0;
        }
        device['targets'][targetHuman]++;
        device.rssi = rssi;
        console.log(`${sourceHuman} => ${targetHuman} at ${rssi}rssi on channel ${channel}`);

        if (this._devices[targetHuman].timer) {
            return;
        }
    }

    _viewDevices(ws, message) {
        if (message.length !== 1) {
            console.log('Message length is incorrect: ' + message.length);

            return;
        }

        const currentlyMonitoring = this._monitors.indexOf(ws);

        if (message[0] && currentlyMonitoring === -1) {
            this._monitors.push(ws);
        } else if (!message[0] && currentlyMonitoring !== -1) {
            this._monitors.splice(currentlyMonitoring, 1);
        } else {
            console.warn('Operation was ignored because current state matches requested.');
        }
    }

    _train(ws, message) {
        if (!ws.whoami) {
            return;
        }

        if (message.length !== 3) {
            console.log('Message length is incorrect: ' + message.length);

            return;
        }

        this._trainingData.push({
            x: message.slice(0, 1).readUInt8(),
            y: message.slice(1, 2).readUInt8(),
            z: message.slice(2, 3).readUInt8(),
            t: (new Date()).getTimestamp(),
        });
    }

    _restart(ws, message) {
        if (message.length !== 0) {
            console.log('Message length is incorrect: ' + message.length);

            return;
        }

        this._broadcastToTrackers(Buffer.from([0x0c]));
    }

    _setChannel(ws, message) {
        if (message.length !== 1) {
            console.log('Message length is incorrect: ' + message.length);

            return;
        }

        this._broadcastToTrackers(Buffer.from([0x0e, message[0]]));
    }

    _broadcastToTrackers(packet) {
        for (let tracker of this._trackers) {
            tracker.send(packet);
        }
    }

    _getDevice(id) {
        id = id.toUpperCase();
        if (!this._devices[id]) {
            this._devices[id] = {
                targets: {},
                rssi: null,
                name: null,
                ip: null,
            };
        }

        return this._devices[id];
    }
}

module.exports = Manager;
