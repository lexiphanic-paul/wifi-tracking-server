const WebSocket = require('ws');
const fs = require('fs');
const Manager = require('./Manager');

const wss = new WebSocket.Server({host: '0.0.0.0',  port: 8081 });
const manager = new Manager(wss);

const http = require('http');
//create a server object:
http.createServer(function (req, res) {
    res.write(fs.readFileSync('index.html')); //write a response to the client
    res.end(); //end the response
}).listen(8080); //the server object listens on port 8080
